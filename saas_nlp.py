# -*- coding: utf-8 -*-
"""
Created on Thu May 27 11:11:10 2021

@author: I0O3
"""

import os
import pandas as pd
import numpy as np
import re

from stopwords import clean
from sentence_transformers import SentenceTransformer, util
from nltk.stem import PorterStemmer

# PATH = os.path.abspath(os.getcwd())
PATH = '.'
# OS_platform = 'WIN10'
# OS_platform ='UNIX'
# if OS_platform == 'WIN10':
#     PATH = PATH+'\\data\\'
# elif OS_platform == 'UNIX':
#     PATH = PATH+'/data/'
PATH = PATH+'/data/'    

file_name = PATH+'clean_df_nlp.csv'
bert_embedding_name = PATH+'bert_embedding.csv'
desc_embedding_name = PATH+'desc_embedding.csv'
cats_unique_list_name = PATH+'cats_unique_list.csv'
bert_model_name = PATH+'all-mpnet-base-v2'
desc_model_name = 'sentence-transformers/msmarco-distilbert-base-v2'


def isNaN(string):
    return string != string


def clean_stop_words_en(value):
    """
    delete english stop words from a given string(value)
    Parameters
        value : english string 
    Returns
        value :no stop words in string
    """
    if value != '':
        value = clean(value.lower().split(), "en")
        value = ' '.join(value)
    return value


def stem_text_en(value):
    """
    do english stemming on a string. used for tfidf similarity
    Parameters
        value : string
    Returns
        value : string, after stemming
    """
    ps = PorterStemmer()
    stemmed_list = list()
    if value != '':
        words_list = value.split()
        for word in words_list:
            stemmed_list.append(ps.stem(word))
        value = ' '.join(stemmed_list)
    return value


def clean_none_char_tokens(valueStr):
    """
    clear none-char[a-z,א-ת] found in a given string.
    delete single-char tokens
    Parameters
        valueStr : string
    Returns
        big_words_str : clean string
    """
    # print(len(valueStr))
    only_chars_str = valueStr
    regex_chars = re.compile('[^a-zA-Z0-9^\u0590-\u05fe ]+')
    only_chars_str = regex_chars.sub(' ', only_chars_str)
    return only_chars_str


def clean_text(text, do_stem=True):
    text0 = clean_none_char_tokens(text)
    text1 = clean_stop_words_en(text0)
    if do_stem:
        text2 = stem_text_en(text1)
        return text2
    else:
        return text1


def prepare_for_similarity(df):
    """
    prepare all tickets for similarity. 
    take only two text-fields. clean the text, delete stop-words
    do english stemming
    Parameters
        df : data-frame
    Returns
        text_list : list of texts from all tickets
    """
    NLP_cols = ['calc_category']
    dfNLP = df[NLP_cols]
    dfNLP = dfNLP.replace(np.nan, '', regex=True)
    text_list = list(dfNLP['calc_category'])

    for idx in range(len(text_list)):
        text_list[idx] = clean_text(text_list[idx])
    return text_list


def process_bert_similarity(to_check, all_embeddings):
    """
    do the process of BERT_similarit. compare to to_check string to all 
    texts in text_list, save the results, sort the results and return the
    3 biggest-similar tickets
    Parameters
        to_check : string to check
        all_embeddings : embeddig of the text-list
    Returns
        similar_tickets : 3 top-similar tickets found
    """
    # if isNaN(to_check[0]):
    if isNaN(to_check):
        return pd.DataFrame(np.zeros((1, 1)))

    # Threshold = 0.794
    model = SentenceTransformer(bert_model_name)
    to_check_embeddings = model.encode(to_check)
    # Compute cosine similarity with all dataset
    cos_sim = util.cos_sim(all_embeddings, to_check_embeddings)
    # convert from tensor to dataframe
    cosine_similarities = pd.DataFrame(cos_sim.numpy())
    # sort similaritis by value (top-down)
    similar_tickets = [cosine_similarities.idxmax()[0],
                       cosine_similarities.max()[0]]
    return similar_tickets


def do_bert_nlp_similarity(new_query):
    """
    called from outside of this module. do all the bert similarity peocess and 
    return the 3-similar tickets.
    Parameters
        new_ticket : dict
    Returns
        similar_tickets : list of three similar tickets.
    """
    bert_embedding, text_list = read_bert_embedding()
    # to_check = prepare_new_ticket_for_similarity(new_ticket)
    similar_results = process_bert_similarity(new_query, bert_embedding)
    return similar_results


def do_bert_desc_similarity(new_query):
    """
    called from outside of this module. do all the bert similarity peocess and 
    return the 3-similar tickets.
    Parameters
        new_ticket : dict
    Returns
        similar_tickets : list of three similar tickets.
    """
    desc_embedding = read_desc_embedding()
    # similar_results = process_bert_similarity(new_query, desc_embedding)
    
    if isNaN(new_query):
        return pd.DataFrame(np.zeros((1, 1)))
    # Threshold = 0.794
    model = SentenceTransformer(desc_model_name)
    to_check_embeddings = model.encode(new_query)
    # Compute cosine similarity with all dataset
    cos_sim = util.cos_sim(desc_embedding, to_check_embeddings)
    # convert from tensor to dataframe
    cosine_similarities = pd.DataFrame(cos_sim.numpy())
    # sort similaritis by value (top-down)
    similar_results = [cosine_similarities.idxmax()[0],
                       cosine_similarities.max()[0]]
    return similar_results


def save_bert_embedding(category_text_unique):
    # text_list = prepare_for_similarity(df)
    cleaned_text = category_text_unique.copy()
    lower_case_text_list = [x.lower() for x in category_text_unique]
    text_unique_list = np.unique(np.array(lower_case_text_list)).tolist()
    text_unique_list.remove('all')
    cleaned_text = [clean_text(x) for x in text_unique_list]

    model = SentenceTransformer(bert_model_name)
    all_embeddings = model.encode(cleaned_text)
    bert_embedding = pd.DataFrame(all_embeddings)
    bert_embedding.to_csv(bert_embedding_name, index=False)
    text_list_df = pd.DataFrame(text_unique_list)
    text_list_df.to_csv(cats_unique_list_name, index=False)    
    return


def save_desc_embedding(descriptions):
    # text_list = prepare_for_similarity(df)
    cleaned_text = descriptions.copy()
    lower_case_text_list = [x.lower() for x in descriptions]
    # text_list = np.array(lower_case_text_list).tolist()
    cleaned_text = [clean_text(x) for x in lower_case_text_list]

    model = SentenceTransformer(desc_model_name)
    all_embeddings = model.encode(cleaned_text)
    desc_embedding = pd.DataFrame(all_embeddings)
    desc_embedding.to_csv(desc_embedding_name, index=False)
    # text_list_df = pd.DataFrame(text_list)
    # text_list_df.to_csv(desc_unique_list_name, index=False)    
    return


def read_desc_embedding():
    desc_embedding = pd.read_csv(desc_embedding_name).values.tolist()
    # cats_unique = pd.read_csv(cats_unique_list_name).values
    # text_list = [x[0] for x in cats_unique]
    return desc_embedding


def read_bert_embedding():
    bert_embedding = pd.read_csv(bert_embedding_name).values.tolist()
    cats_unique = pd.read_csv(cats_unique_list_name).values
    text_list = [x[0] for x in cats_unique]
    return bert_embedding, text_list, cats_unique


if __name__ == "__main__":
    # bert_embedding, text_list = read_bert_embedding()
    # new_query = 'QuickBooks'
    # similar_results = do_bert_nlp_similarity(new_query)

    new_query = 'Square Document Automation'
    similar_results = do_bert_desc_similarity(new_query)

    print('complete NLP')
