# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 12:10:18 2022

@author: reuve
"""

import pandas as pd
import json


def main_categories_for_saas(saas):
    """
    make the mainCategories field for a given saas.
    The mainCategories is a list of [0..3] strings

    Parameters
    ----------
        saas : string. json string of the saas record

    Returns
    -------
        mainCategories: list of 0..3 strings of categories
    """
    
    def desc_as_string(desc_list):
        """
        cancat all descriptions in the given list to single string

        Parameters
        ----------
            desc_list : list of strings. each string is a description

        Returns
        -------
            desc_str : string. a string consist of all the descriptions in the given list
        """
        desc_str = ''
        if len(desc_list) <= 0:
            return desc_str
        for desc in desc_list:
            if 'description' in desc:
                desc_str += ' '+ desc['description']      
        return desc_str


    def clean_string(txt):
        """
        clean a given string from un-needed chars

        Parameters
        ----------
            txt : string

        Returns
        -------
            cleaned_text : string. without the 'dirty' chars

        """
        cleaned_text = txt.lower()  
        for ch in [ '\n', ',', "'", '.', '(', ')', '’', '&']:
            if ch in cleaned_text:
                cleaned_text = cleaned_text.replace(ch,' ')
        cleaned_text = " ".join(cleaned_text.split())     
        return cleaned_text  


    def make_token_frequency_list(token_list, desc_str, cats_str):
        """
        calcaulte for each token in token list, calculate its frequency 

        Parameters
        ----------
            token_list : list of strings. the given token list
            desc_str : string. the concatination of all descriptions
            cats_str : string. the concatination of all existing categories.
        Returns
        -------
            TYPE: dataframe. 2 columes: token and frequescy

        """
        token_freq_list = []
        for token in token_list:
            if len(token) == 2:
                n_in_desc = desc_str.count(' '+token+' ')
            else:
                n_in_desc = desc_str.count(token)           
            n_freq = n_in_desc + cats_str.count(token)
            item = {'token':token, 'freq':n_freq}
            token_freq_list.append(item)
        return pd.DataFrame(token_freq_list)

    
    def clean_list(lst):
        """
        clean a given list from 'dirty' words

        Parameters
        ----------
            lst : list of strings
        
        Returns
        -------
            new_lst : cleaned list of strings.

        """
        to_delete = ['&','software','system','systems','for','and','tool','to','all'
                 'tools','management','of','other','the','top','you','with','as']
        new_lst = lst
        for item in new_lst:
            if (item in to_delete) or (len(item)<2) :
                new_lst.remove(item)
        return new_lst

# start of main function
    # in case of single category      
    if len(saas["categories"]) == 1:
       return saas["categories"]
    
    # make the description token list        
    mainCategories = []    
    desc_str = desc_as_string(saas['descriptionsList'])
    desc_str_clean = clean_string(desc_str)    
    desc_token_list = list(set(desc_str_clean.split(' ')))
    desc_token_list = clean_list(desc_token_list)

    # make the categories token list        
    cats_token_list = []
    if len(saas["categories"]) > 0:
        all_cats_string = " ".join([str(item).lower() for item in saas["categories"]])
        all_cats_string = clean_string(all_cats_string)    
        cats_token_list = list(set(all_cats_string.split(' '))) 
        cats_token_list = clean_list(cats_token_list)            
        if (len(cats_token_list) == 1) and (cats_token_list[0] == ''):
            cats_token_list = []

    # make list of tokens found in both lists                
    token_both = list((set(cats_token_list)).intersection(set(desc_token_list)))

    # if there are tokens found in both lists
    if len(token_both) > 0:
        # if list is short
        if len(token_both) <= 3:
            mainCategories = token_both
        # list is long. calculate the frequescy and take the token with most-frequencies
        else:
            token_freq_df = make_token_frequency_list(token_both, desc_str_clean, all_cats_string)
            mainCategories = list(token_freq_df.nlargest(3, 'freq', keep='first')['token']) 
    # no tokens in category tokens  list
    elif (len(cats_token_list) <= 0):
        mainCategories = []
    
    # there are tokens in category list. calculate the frequescy and take the token with most-frequencies
    elif (len(cats_token_list) > 0):
            token_freq_df = make_token_frequency_list(cats_token_list, [], all_cats_string)
            mainCategories = list(token_freq_df.nlargest(3, 'freq', keep='first')['token'])
    return mainCategories

  
if __name__ == '__main__':
    with open('saas_json.txt') as json_file:
        data = json.load(json_file)    
    main_categories_for_saas(data)  
    print('end of work')    
