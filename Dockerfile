FROM python:3.8-slim as builder
RUN pip install --user fastapi uvicorn boto3

# By default, listen on port 5000
EXPOSE 5000/tcp

# Set the working directory in the container
WORKDIR /app

# Copy the dependencies file to the working directory
#COPY requirements.txt .
COPY . .

# Install any dependencies
# RUN pip install --no-cache-dir -r requirements.txt

# Copy the content of the local src directory to the working directory
#COPY flask_server.py .

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir jinja2
RUN pip install --no-cache-dir flask==2.1.1
RUN pip install --no-cache-dir pandas
RUN pip install --no-cache-dir requests
RUN pip install --no-cache-dir elasticsearch==7.16.3
RUN pip install --no-cache-dir stopwords
RUN pip install --no-cache-dir torch==1.11.0
RUN pip install --no-cache-dir transformers==4.20.0
RUN pip install --no-cache-dir sentence_transformers==2.2.2
RUN pip install -U flask-cors

CMD ["python3", "flask_server.py"]