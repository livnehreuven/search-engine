# -*- coding: utf-8 -*-
"""
Created on Tue May 24 12:19:17 2022

@author: reuve
"""

# Python program to translate
# speech to text and text to speech
# based on https://www.geeksforgeeks.org/python-convert-speech-to-text-and-text-to-speech/

import speech_recognition as sr
import pyttsx3

# Function to convert text to
# speech
def SpeakText(command):
	
	# Initialize the engine
	engine = pyttsx3.init()
	engine.say(command)
	engine.runAndWait()

	
def stt():
    
    # Initialize the recognizer
    r = sr.Recognizer()
    # while(1):    	
   	# Exception handling to handle
   	# exceptions at the runtime
    try:   		
   		# use the microphone as source for input.
   		with sr.Microphone() as source2:
   			
   			# wait for a second to let the recognizer
   			# adjust the energy threshold based on
   			# the surrounding noise level
   			r.adjust_for_ambient_noise(source2, duration=0.2)
   			
   			#listens for the user's input
   			audio2 = r.listen(source2)
   			
   			# Using google to recognize audio
   			MyText = r.recognize_google(audio2)
   			MyText = MyText.lower()
   
   			print("text: "+MyText)
   			SpeakText(MyText)
   			
    except sr.RequestError as e:
   		print("Could not request results; {0}".format(e))
   		
    except sr.UnknownValueError:
   		print("unknown error occured")
    return MyText
    
 
if __name__ == '__main__':
    # cont = True
    # while(cont):
    print("speek... ")
    myText = stt()
    print('end of work') 
