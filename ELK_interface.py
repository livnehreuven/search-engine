# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 10:12:52 2022ע

@author: reuve
"""

import time
import requests

from elasticsearch import Elasticsearch
from elasticsearch import helpers


headers = {'Content-Type': 'application/json','Connection':'close'}
# AUTH=('aviad@cloudyo.io','cloudyoIO@1') 
AUTH=('reuven@cloudyo.io','asdZXC123') 
  
TO_SLEEP = 0

base_elasticsearch_url = "https://cloudyo.es.us-central1.gcp.cloud.es.io:9243/{0}/_search"
es_url ="https://cloudyo.es.us-central1.gcp.cloud.es.io:9243"


def elk_close():
    es.close()
    return


def elk_init():
    """
    init the elastic DB. create the elasticsearch object 

    Returns
    -------
        es : elastic search object
    """
    global es
    es = Elasticsearch([es_url],
                   http_auth=AUTH,
                   verify_certs=True,
                   maxsize=2,
                   http_compress=True,           
                   timeout=800
                   )
                   # sniff_on_start=True,
                   # sniff_on_connection_fail=True)
                   #  # sniffer_timeout=20)
    # es_client = Elasticsearch(http_compress=True)           
    return es

ELK_key_lookup = {
    'Customers' : 'typicalCustomers',
    'Platforms' : 'platformsSupported',
    'Support' : 'supportOptions',
    'Training' : 'trainingOptions'
    }


def elk_request_get(index_url, headers, offload):
    """
    do the proper ELK request based on the given parameters.
    response consist all the data
    Parameters
        index_url : string. ELK index
        headers : small dict. a const defined above
        offload : string. the ELK query
    Returns
        resp: int.  0 in success,
                    negativ value in error.
        data: dict. response data if success
                    {} - in error
    """ 
    global es
    
    index_name = index_url.split('/')[3]
    if (TO_SLEEP>0):
        time.sleep(TO_SLEEP)
    try:
        response = es.search(body=offload, 
                             index=index_name, 
                             headers=headers)
                              # _source=True)     
        if 'hits' in response.keys():
            if len(response['hits']['hits']) > 0:  # Check if hits returned
                data = response['hits']['hits']
                # response.close()
                return 0, data
            else:
                # response.close()
                return -4, {}
        else:
            # response.close()
            return -6, {}
    except requests.exceptions.RequestException as e:  # This is the correct syntax
        raise SystemExit(e)
        # response.close()
        return -8, {}


def convert_filters_to_query(filters):
    """
    convert the filters defined by the frontend user to ELK search query.
    for each filter-type: 
        if single value then it is 'must'
        else (its list): they are all 'should'

    Parameters
    ----------
    filters : string
        in the following format:
            type1 = value_1,...,value_n & type2=value_i...

    Returns
    -------
    TYPE: string
        the 'must' and 'should' parts

    """
    
    must_query = ''
    should_query = ''
    if len(filters) <= 0:
        return(']')
    else:
    # if len(filters) > 0:
        # define filter: each entry is adefinition of filter type
        if '&' in filters:
            filters_list = filters.split('&')
        else: 
            # single item in list
            filters_list =[filters]
        # loop over all the filter categories
        for filter_def in filters_list:
            # type = list of values
            if '=' in filter_def:
                filter_line = filter_def.split('=')
                # index 0 is the filter-type
                filter_type = ELK_key_lookup[filter_line[0]]
                # index 1 is the value list.
                filter_values = filter_line[1].split(',')
                # single value in list. it is in 'must' part
                if len(filter_values) == 1:
                    # if its 'all' we need to make regular expression of '.*' 
                    if filter_values[0] == 'all': 
                        must_query = must_query + '  ,{"regexp": {"'+filter_type+'": {"value": ".*" }}}'
                    else:       
                        must_query = must_query + '  ,{"match": {"'+filter_type+'" : "'+filter_values[0]+'"}}'
                # many values in list: it is in 'should' part
                elif len(filter_values) > 1:
                    for filter_val in filter_values:
                        should_query = should_query + '  {"match": {"'+filter_type+'" : "'+filter_val+'"}},'
        # here 'must' and 'should' parts are ready
        # close the 'must' part
        must_query = must_query + '],'
        # 'should' part exist??
        if len(should_query) > 0:
            # remove last ',' sign
            should_query = should_query[:-1]
            # start and close the 'should part
            should_query = '"should":[' + should_query + ']' 
        else:
            must_query = must_query[:-1]          
    return must_query+should_query


def get_All_from_index(index_name):
    """
    get all records from a given elk index
    Parameters
        index_name : string. the elk index
    Returns
        resp : 0 if success, negative value in case of error
        data : dictionary. the data read from ELK, 
    """
    index_url = base_elasticsearch_url.format(index_name)
    offload = '{"size": 30000,'\
        '"query": {'\
        '"match_all": {}}}'
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data   


def get_all_with_field(field): 
    """
    get all reacords with a given field from 'services' elk index 
    Parameters
        field : string. the fneeded field
    Returns
        resp : 0 if success, negative value in case of error
        data : dictionary. the data read from ELK, 
    """
    index_url = base_elasticsearch_url.format('unified-data')
    # '    {"match_all": {}}'\
    
    offload = '{"size": 30000,'\
    '  "_source": ["docId", "saasNameForSearch","' +field+'"],'\
    '  "query": {"bool": {"must" : ['\
    '    {"exists" : {"field": "'+field+'"}}'\
    '    ]}}}'  
     
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data


def get_docId_alternatives():
    """
    read-in all 'services-alternatives' elk index.
    nedd to check if 30000 is porpare (maybe increase it??)

    Returns
    -------
    resp : integer
        the response code.
    data : dictionary 
        the data read from the ELK.
    """
    index_url = base_elasticsearch_url.format('services-alternatives')  
    offload = '{"size": 30000,'\
    '  "_source": "docId",'\
    '  "query": {"bool": {"must" : ['\
    '    ]}}}'   
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data


def get_categories_unique():
    """
    read-in all 'categories_unique' elk index. currently its 1005 long

    Returns
    -------
    resp : integer
        the response code.
    data : dictionary 
        the data read from the ELK.
    """
    index_url = base_elasticsearch_url.format('categories_unique')  
    offload = '{"size": 2000,'\
    '  "_source": ["category"],'\
    '  "query": {"bool": {"must" : ['\
    '    ]}}}'   
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data


def get_all_without_field(field):
    """
    get all reacords without a given field from 'services' elk index 
    Parameters
        field : string. the fneeded field
    Returns
        resp : 0 if success, negative value in case of error
        data : dictionary. the data read from ELK, 
    """
    index_url = base_elasticsearch_url.format('services')  
    offload = '{"size": 20000,'\
    '  "query": {"bool": {"must_not" : ['\
    '    {"exists" : {"field": "'+field+'"}}'\
    '    ]}}}'   
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    


def get_data_by_docId(docId,index):
    """
    get the record with 'docId' value from a given elk index.
    docId is a unique key for this record.
    Parameters
        docId : string. the unique key 
        index : string. the elk index name
    Returns
        resp : 0 if success, negative value in case of error
        data : dictionary. the data read from ELK, 
    """
    index_url = base_elasticsearch_url.format(index)
    offload = '{"size": 2,'\
    '  "query": {"bool": {"must" : ['\
    '   {"match": {"docId" : "'+docId+'"}}'\
    '    ]}}}'  
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    


def get_alternatives(provider,saas,category):
    """
    get alternatives for a giver saas,provider and docId    

    Parameters
    ----------
    provider : string
        must be from this provider.
    saas : string
        must be with this saas name.
    category : list of string
        get the docId from the category( 1-2 entries)

    Returns
    -------
    resp : integer
        response code.
    data : dictoionary 
        data read from the ELK

    """
    index_url = base_elasticsearch_url.format('services-alternatives')
    if 1 == len(category): 
        offload = '{"size": 1,'\
        '  "query": {"bool": {"must" : ['\
        '   {"match": { "docId" : "'+category[0]+'"}},'\
        '   {"match": { "providerForSearch": "'+provider+'"}},'\
        '   {"match": { "saasNameForSearch": "'+saas+'"}}'\
        '  ]}}}' 
    elif 2 == len(category):
        offload = '{"size": 1,'\
        '  "query": {"bool": {"must" : ['\
        '   {"match": { "docId" : "'+category[0]+'"}},'\
        '   {"match": { "docId" : "'+category[1]+'"}},'\
        '   {"match": { "providerForSearch": "'+provider+'"}},'\
        '   {"match": { "saasNameForSearch": "'+saas+'"}}'\
        '  ]}}}'         
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    
 
    
def get_alternatives_by_saas(saas):
    """
    get alternatives for a given saas
 
    Parameters
    ----------
    saas : string
        must be with this saas name.
        
    Returns
    -------
    resp : integer
        response code.
    data : dictoionary 
        data read from the ELK
 
    """
    index_url = base_elasticsearch_url.format('services-alternatives')
    offload = '{"size": 10000,'\
    '  "query": {"bool": {"must" : ['\
    '   {"match": { "saasNameForSearch" : "'+saas+'"}}'\
    '  ]}}}'
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    
     
    
def get_data_by_categories(cats, filters):
    """
    get data from 'unified-data' index with given categories.

    Parameters
    ----------
    cats : list of strings(1-2 entries)
        the requested catogories. each one is a word (no blanks)

    Returns
    -------
    resp : integer
       response code.
    data : dictoionary 
       data read from the ELK
    """
    # index_url = base_elasticsearch_url.format('services')
    index_url = base_elasticsearch_url.format('unified-data')
    filters_query = convert_filters_to_query(filters)
 
    if 1 == len(cats): 
        offload = '{"size": 10000,'\
        '  "query": {"bool": {"must" : ['\
        '  {"match": {"mainCategories" : "'+cats[0]+'"}}'+filters_query+'    }}}'
    elif 2 == len(cats):
        offload = '{"size": 10000,'\
        '  "query": {"bool": {"must" : ['\
        '  {"match": {"mainCategories" : "'+cats[0]+'"}},'\
        '  {"match": {"mainCategories" : "'+cats[1]+'"}}'+filters_query+'    }}}'
    elif 3 <= len(cats):
       offload = '{"size": 10000,'\
       '  "query": {"bool": {"must" : ['\
       '  {"match": {"mainCategories" : "'+cats[0]+'"}},'\
       '  {"match": {"mainCategories" : "'+cats[1]+'"}},'\
       '  {"match": {"mainCategories" : "'+cats[2]+'"}}'+filters_query+'    }}}'
    else:
        resp = -8
        data = {}

    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    
 

def get_all_categories():
    index_url = base_elasticsearch_url.format('unified-data')
    offload = '{"size": 30000, '\
      '"_source": ["docId","categories"],'\
      '"query": {"bool": {"must" : ['\
       ' ]}}}'
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data


def get_all_descriptions():
    index_url = base_elasticsearch_url.format('unified-data')
    offload = '{"size": 10, '\
      '"_source": ["docId","descriptionsList"],'\
      '"query": {"bool": {"must" : ['\
       ' ]}}}'
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data


def get_all_descriptions_cats():
    index_url = base_elasticsearch_url.format('unified-data')
    offload = '{"size": 30000, '\
      '"_source": ["docId","descriptionsList","categories"],'\
      '"query": {"bool": {"must" : ['\
       ' ]}}}'
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data


def get_data_by_saasName(saas,filters):
    """
    get data from 'unified-data' index with given saas-name.

    Parameters
    ----------
    saas : list of strings
        the requested saas. each one is a word (no blanks)

    Returns
    -------
    resp : integer
       response code.
    data : dictoionary 
       data read from the ELK

    """
    filters_query = convert_filters_to_query(filters)
    # index_url = base_elasticsearch_url.format('services')
    index_url = base_elasticsearch_url.format('unified-data')
    # offload = offload+'  {"match": {"saasNameForSearch" : "'+saas+'"}}'

    offload = '{"size": 10000,'\
    '  "query": {"bool": {"must" : ['\
    '  {"match": {"docId" : "'+saas+'"}}'+filters_query+'    }}}'
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    


def get_sass_by_docId_saasName(docId,saasName):
    index_url = base_elasticsearch_url.format('services')
    offload = '{"size": 10000,'\
    '  "query": {"bool": {"must" : ['\
    '  {"match": {"docId" : "'+docId+'"}},'\
    '  {"match": {"saasNameForSearch" : "'+saasName+'"}}'\
    '    ]}}}' 
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    
    

def get_price_data_by_saasName(docId, saas):
    index_url = base_elasticsearch_url.format('services-price')
    offload = '{"size": 10,'\
    '  "query": {"bool": {"must" : ['\
    '  {"match": {"docId" : "'+docId+'"}},'\
    '  {"match": {"saasNameForSearch" : "'+saas+'"}}'\
    '  ]}}}' 
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    


def get_integrations_by_saasName(docId, saas):
    index_url = base_elasticsearch_url.format('services')
    offload = '{"size": 10,'\
    '  "_source": ["docId", "saasNameForSearch","saasIntegrations"],'\
    '  "query": {"bool": {"must" : ['\
    '  {"match": {"docId" : "'+docId+'"}},'\
    '  {"match": {"saasNameForSearch" : "'+saas+'"}}'\
    '  ]}}}' 
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    


def get_logo_and_url_elk(offload):
    """
    Get logs and urls from the special elk index using the given query.
    
    Parameters
    ----------
    offload : string. the elk query

    Returns
    -------
    resp : integer
       response code.
    data : dictoionary 
       data read from the ELK
    """
    index_url = base_elasticsearch_url.format('urls-and-logos')
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    

 
def get_saas_by_url(url):
    offload = '{ "size" : 100, "query": {"bool": {"must" : ['\
        '{"match": {"saasUrl":"'+url+'"}}'\
        ']}}}'
    index_url = base_elasticsearch_url.format('urls-and-logos')
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data    
      
    
def put_embedding_data(df):
    for idx in range(len(df)):
        line_json = df.loc[idx].to_json()       
        ret_code = es.index(index='services-embedding',
                        doc_type='external',
                        id=idx,
                        body=line_json)
        print(idx)
    return ret_code


def get_embedding_data():
    """
    get bert embedding data from ELK index 'services-embedding'

    Returns
    -------
    resp : integer
       response code.
    data : dictoionary 
       data read from the ELK
 
    """
    index_url = base_elasticsearch_url.format('services-embedding')
    offload = '{"size": 2000,'\
    ' "query": {"bool": {"must" : ['\
    '   ]}}}'
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data        
    

def post_df_to_elk(df):
    # use_these_keys = ['text', 'categories']  
    # def filterKeys(document):
    #     return {key: document[key] for key in use_these_keys }    
    def doc_generator(df):
        df_iter = df.iterrows()
        for index, document in df_iter:
            yield {
                    "_index": 'test1',
                    "_type": "_doc",
                    "_id" : f"{document['text']}",
                    "_source": use_these_keys
                }
        raise StopIteration
        return
    
    use_these_keys = list(df.keys())
    if len(df) > 500:
        idx = 0
        while idx < len(df):
            temp_df = df[idx:min(len(df),idx+500)]
            ret = helpers.bulk(es, doc_generator(temp_df))
            idx = idx+500
            print(ret)         
    else: 
        ret = helpers.bulk(es, doc_generator(df))   
    return ret


def get_fields_by_name(saas_name, fields):
    index_url = base_elasticsearch_url.format('unified-data')
    offload = '{"size": 1,  "_source": ['+fields+'],' \
                                                 '  "query": {"bool": {"must" : [' \
                                                 '  {"match": {"docId" : "'+saas_name+'"}} ]}}}'
    resp, data = elk_request_get(index_url, headers, offload)
    return resp,data


if __name__== "__main__":
    elk_init()    
    elk_close()
    print ('complete ELK')

    