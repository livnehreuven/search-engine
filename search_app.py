# -*- coding: utf-8 -*-
"""
Created on Tue Nov  2 16:18:34 2021

@author: reuve
"""

import json

import pandas as pd
import numpy as np
import math

# from autocorrect import Speller

# from random import randrange
from saas_nlp import clean_stop_words_en

from ELK_interface import get_fields_by_name,get_all_with_field,get_All_from_index,get_data_by_docId,get_data_by_categories,get_all_without_field,\
    post_df_to_elk, elk_init,elk_close,put_embedding_data,get_embedding_data,get_logo_and_url_elk,\
    get_docId_alternatives,get_categories_unique,get_alternatives,get_alternatives_by_saas,get_data_by_saasName,\
    get_price_data_by_saasName,get_integrations_by_saasName,get_all_categories,get_all_descriptions,get_all_descriptions_cats

from saas_nlp import save_bert_embedding,read_bert_embedding,process_bert_similarity,clean_text
# EMBEDDING_MODE = True
EMBEDDING_MODE = False
score_threshold = 0.7
results_length = 12


def search_saas_by_name(name, fields):
    price_dict = {}
    resp, data = get_fields_by_name(name, fields)
    if resp < 0:
        print ("*** ERROR {} in get_price_data_by_saasName {}".format(resp, 'services'))
        return price_dict
    price_dict = data[0]['_source']
    return price_dict


def prepare_with_field(field):
    """
    get data-records from 'services' ELK index that consist with spcific field
    Parameters
        field : string. the needed field
    Returns
        data_df : dataframe.
    """
    data_df = pd.DataFrame()  
    resp, data = get_all_with_field(field)
    if resp < 0:
        print ("*** ERROR {} in get_all_with_field {}".format(resp, field))
        return data_df
    # loop over the read data. take only the '_source' sub-dictionary.
    data_list = []
    for idx in range(len(data)):
        data_list.append(data[idx]['_source'])
    # convert to a datafrmae
    data_df = pd.DataFrame(data_list)    
    return data_df


def prepare_categories_from_alternatives():
    """
    get the docId from all data-records from 'alternatives' ELK index 
    Parameters
        none
    Returns
        data_list :list of strings
    """
    data_df = pd.DataFrame()  
    resp, data = get_docId_alternatives()
    if resp < 0:
        print ("*** ERROR {} in get_docId_alternative".format(resp))
        return data_df
    # loop over the read data. take only the '_source' sub-dictionary.
    data_list = [ data[idx]['_source']['docId'] for idx in range(len(data)) ]
    return data_list    


def prepare_categories_unique():
    """
    get all the categories from categories_unique elk index

    Returns
    -------
    TYPE: list of strins

    """
    data_df = pd.DataFrame()  
    resp, data = get_categories_unique()
    if resp < 0:
        print ("*** ERROR {} in get_categories_unique".format(resp))
        return data_df
    # loop over the read data. take only the '_source' sub-dictionary.
    data_list = [ data[idx]['_source']['category'] for idx in range(len(data)) ]
    return data_list        


def prepare_without_field(field):
    """
    get data-records from 'services' ELK index that consist with spcific field
    Parameters
        field : string. the needed field
    Returns
        data_df : dataframe.
    """
    data_df = pd.DataFrame()  
    resp, data = get_all_without_field(field)
    if resp < 0:
        print ("*** ERROR {} in get_all_with_field {}".format(resp, field))
        return data_df
    # loop over the read data. take only the '_source' sub-dictionary.
    data_list = []
    for idx in range(len(data)):
        data_list.append(data[idx]['_source'])
    # convert to a datafrmae
    data_df = pd.DataFrame(data_list)    
    return data_df   


def prepare_by_docId(docId,index):
    """
    get a record from the ELK which is stored in a given ELK index
    and its docId is as the given docId
    Parameters
        docId : string. the uniqe record-key
        index : string. the ELK index
    Returns
        data_dict : the needed record.
    """
    data_dict = {}  
    resp, data = get_data_by_docId(docId,index)
    if resp < 0:
        print ("*** ERROR {} in get_data_by_docId {}".format(resp, index))
        return data_dict
    data_dict = data[0]['_source']
    return data_dict   
       

def prepare_alternatives(provider,saas,category):
    """
    get alternatives limited to input parameters (and operation)

    Parameters
    ----------
    provider : string
        the saas provider.
    saas : string
        the saas name.
    category : string
        the category.

    Returns
    -------
    data_dict : dictionary
    """
    data_dict = {}      
    resp, data = get_alternatives(provider,saas,category)
    if resp < 0:
        print ("*** ERROR {} in get_data_by_docId {}".format(resp, ''))
        return data_dict
    data_dict = data[0]['_source'] 
    return data_dict
       

def prepare_alternatives_by_saas(saas):
    """
    get alternatives limited to saas name

    Parameters
    ----------
    saas : string
        the saas name.
    Returns
    -------
    data : dictionary

    """    
    data_dict = {}      
    resp, data = get_alternatives_by_saas(saas)
    if resp < 0:
        print ("*** ERROR {} in get_alternatives_by_saas {}".format(resp, ''))
        return data_dict
    # data_dict = data[0]['_source']
    return data
    

def prepare_by_services(saas, filters):
     """
     get services by saas name
 
     Parameters
     ----------
     saas : string
         the saas name.
     Returns
     -------
     data_list : list of dictionaries
  
     """           
     data_list = [] 
     # filter_dict = filter_parse(filters)     
     resp, data = get_data_by_saasName(saas, filters)
     # resp, data = get_data_by_saasName(saas)
     if resp < 0:
         print ("*** ERROR {} in get_data_by_saas {}".format(resp, 'services'))
         return data_list
     for idx in range(len(data)):
         data_list.append(data[idx]['_source'])
     return data_list    


def prepare_price_by_saas(docId,saas):
     price_dict = {}
     resp, data = get_price_data_by_saasName(docId,saas)
     if resp < 0:
         print ("*** ERROR {} in get_price_data_by_saasName {}".format(resp, 'services'))
         return price_dict
     price_dict = data[0]['_source']['pricePlans']
     return price_dict    
     

def prepare_integrations_by_saas(docId, saas):
     price_dict = {}
     resp, data = get_integrations_by_saasName(docId,saas)
     if resp < 0:
         print ("*** ERROR {} inget_integrations_by_saasName {}".format(resp, 'services'))
         return price_dict
     integrations_list = data[0]['_source']['saasIntegrations']
     return integrations_list   

  
def prepare_by_categories(cats, filters):
    """
    get services by category

    Parameters
    ----------
    cats : list of strings
        the category
    Returns
    -------
    data_list : list of dictionaries
 
    """ 
    data_list = [] 
    resp, data = get_data_by_categories(cats, filters)
    if resp < 0:
        print ("*** ERROR {} in get_data_by_categories {}".format(resp, 'services'))
        return data_list
    for idx in range(len(data)):
        data_list.append(data[idx]['_source'])
    return data_list       


def prepare_all_categories():
    data_list = []       
    resp, data = get_all_categories()
    if resp < 0:
        print ("*** ERROR {} in get_all_categories".format(resp))
        return data_list
    for idx in range(len(data)):
        # data_list.append(data[idx]['_source']['categories'])
        data_list.append(data[idx]['_source'])      
    return data_list         
    

def prepare_all_descriptions():
    data_list = []       
    resp, data = get_all_descriptions()
    if resp < 0:
        print ("*** ERROR {} in get_all_categories".format(resp))
        return data_list
    for idx in range(len(data)):
        # data_list.append(data[idx]['_source']['categories'])
        data_list.append(data[idx]['_source'])      
    return data_list         
    

def prepare_all_descriptions_cats():
    data_list = []       
    resp, data = get_all_descriptions_cats()
    if resp < 0:
        print ("*** ERROR {} in get_all_descriptions_cats".format(resp))
        return data_list
    for idx in range(len(data)):
        # data_list.append(data[idx]['_source']['categories'])
        data_list.append(data[idx]['_source'])      
    return data_list         
  


     
def prepare_services_data(index_name):
    """
    get all data from a given ELK index
    Parameters
        index_name : string.  ELK index
    Returns
        data_df : dataframe
    """
    data_df = pd.DataFrame()  
    resp, data = get_All_from_index(index_name)
    if resp < 0:
        print ("*** ERROR {} in get_All_from_index {}".format(resp, index_name))
        return data_df
    data_list = []
    for idx in range(len(data)):
        data_list.append(data[idx]['_source'])
    data_df = pd.DataFrame(data_list)    
    return data_df   


# def prepair_uniques():
#     # get all searvices that has 'categories' field
#     services_df = prepare_with_field('categories')
#     cats_string_list,categories_unique_list = calc_category_unique(services_df)        
#     return cats_string_list   
      

# def prepair_uniques_alternatives():    
#     docId_list = prepare_categories_from_alternatives()
#     cats_string_list,categories_unique_list = calc_category_unique_alter(docId_list)           
#     return
    

# def prepare_embedding_data():
#     data_list = []
#     resp,data = get_embedding_data()
#     if resp < 0:
#         print ("*** ERROR {} in get_embedding_data {}".format(resp, 'services-embedding'))
#         return data_list
#     for idx in range(len(data)):
#         data_list.append(list(data[idx]['_source'].values()))
#     return data_list     


def calc_category_unique(services_data_df):
    cats_string_list = []
    cats_set = set()
    categories_list = services_data_df['categories'].to_list()
    categories_unique_list = np.unique(np.array(categories_list)).tolist()
    # if item in categories_unique_list has two values
    # we need to handel each value as a unique
    for idx in range(len(categories_unique_list)):
        item = categories_unique_list[idx]
        if len(item) == 1:
            cats_set.add(item[0])
        elif len(item) == 2:
            cats_set.add(item[0])
            cats_set.add(item[1])
    # now we add the new values to the original list       
    for item in cats_set:
        categories_unique_list.append([item])
        
    for idx in range(len(categories_unique_list)):
        cats_string_list.append(' '.join(categories_unique_list[idx]))          
    return cats_string_list,categories_unique_list
    
   
def calc_category_unique_alter(docId_list):
    docId_unique_list = list(np.unique(np.array(docId_list)))
    no_cats_count = 0
    # cats_list  = []
    cats_set = set()
    for idx in range(len(docId_unique_list)):
        item = docId_unique_list[idx].split('-')[-1]
        if '[' in item:
            only_cats = item.split('[')[1]
            cats = only_cats.split(']')[0]
            if ',' in cats:
                cats_set.add(cats.split(',')[0])
                cats_set.add(cats.split(',')[1])
                cats_set.add(cats.replace(',', ' '))
            else:
                cats_set.add(cats)
        else:
           no_cats_count +=1 
    cats_string_list = list(cats_set)
    return cats_string_list
    

# def read_query_list():
#     """
#     read the query_list for testing. 
#     this list consist all the services from the spec. file : 'SaaS Categories.docx'
#     Returns
#         query_list : list of strings
#     """
#     f = open("query_list.txt", "r")
#     string = f.read()
#     f.close()
#     query_list = string.split('\n')
#     # for idx in range(len(query_list)):
#     #     query_list[idx] = query_list[idx].replace(' Software','')
#     return query_list
    
 
def write_query_list(query_list):
    textfile = open("query_list.txt", "w")
    for element in query_list:
        textfile.write(element + "\n")
    textfile.close() 
    return
    
    
def isNaN(num):
    return num!= num


def save_embedding():
    elk_init()
    category_text_unique = prepare_categories_unique()  
    elk_close()
    save_bert_embedding(category_text_unique)
    return

"""
def test_category_search(bert_embedding,text_list):
    query_list = prepare_query_list()
    results = []
    for idx in range(len(query_list)):
        new_query = clean_text(query_list[idx])
        similar_results = process_bert_similarity(new_query,bert_embedding)
        item = [query_list[idx],
                text_list[similar_results[0]],
                similar_results[1]]
        results.append(item)
        print(idx)
    df = pd.DataFrame(results)
    df = df.rename(columns={0: "query", 1: "ELK text", 2: "bert score"})
    df.to_csv('query_results.csv')
    return    


def test_1():
    data_to_check_df  = pd.read_csv('SAAS_descriptions_to_check.csv')
    saas_desc = list(services_data_df['description'])
    to_check_desc = list(data_to_check_df['_source.description'])
    cats = []
    docIds = []
    for idx in range(len(to_check_desc)):
        desc = to_check_desc[idx]
        if isNaN(desc):
           cats.append([])
           docIds.append('')            
        elif desc in saas_desc:
            # caterory = data_to_check_df.loc[idx]['category']
            # if idx == 2:
            #     print('2')
            saas_idx = services_data_df[services_data_df['description'] == desc].index[0]
            cats.append(services_data_df.loc[saas_idx].categories)
            docIds.append(services_data_df.loc[saas_idx].docId)
        else:
            cats.append([])
            docIds.append('')
    data_to_check_df['docId'] = docIds
    data_to_check_df['categories'] = cats
    data_to_check_df.to_csv('SAAS_desc_to_check_processed.csv')
    return
"""


def prepare_service_list_by_weight(services_list):
    """
    Convert the given searvice list to dataframe with weight and rating for each

    Parameters
    ----------
        services_list : list of dicts

    Returns
    -------
        services_df : updated dataframe of services

    """
    # convert input to dataframe
    services_df = pd.DataFrame(services_list)
    # init lists
    weights = []
    ratings = []
    numberOfVotes_list = []   
    # loop over all the services
    for idx in range(len(services_df)):
        # print(idx)
        cur_service = services_df.loc[idx]
        # get the rating
        rating = 0.0
        # if 'rating' in cur_service:
        if 'calculatedRatingFromAllProviders' in cur_service:      
            if not isNaN(cur_service['calculatedRatingFromAllProviders']):
                rating = float(cur_service['calculatedRatingFromAllProviders'])
        # save rating in list
        ratings.append(rating)
        # n_cats = len(cur_service['categories'])
        
        # get number of votes
        numberOfVotes = 0     
        for votes in cur_service['votesList']:
            if not isNaN(votes['numOfVotes']):
                numberOfVotes += numberOfVotes_to_number(votes['numOfVotes'])
        
        # save number of votes and weights in lists
        numberOfVotes_list.append(numberOfVotes)
        # if (n_cats>0):        
        #     numberOfVotes = numberOfVotes/n_cats
        # weights = avg.rating * log(numberOfVotes)
        # if n_cats > 0:
        #     numberOfVotes = numberOfVotes/n_cats
        if numberOfVotes <= 2:
            weights.append(rating)
        else:
            weights.append(rating * math.log(numberOfVotes,2))
            
     
    # save the new lists in the dataframe
    services_df['rating'] = ratings
    services_df['numberOfVotes'] = numberOfVotes_list      
    services_df['weight'] = weights
    # remove updateDate column since it may have Nan values.
    if 'updateDate' in services_df.keys():
        del services_df['updateDate']
    # sort by weight
    services_df = services_df.sort_values(by=['weight'], ascending=False) 
    services_df = services_df.reset_index(drop=True)      
    return services_df
      

def query_in_string(query,a_string):
    """
    check if the user query tokens found in given string ('description' for example)

    Parameters
    ----------
        query : string. user query
        a_string : most times: the service description
        
    Returns
    -------
        bool: True if query tokens in the string

    """
    stemed_query = clean_text(query)
    query_tokens = stemed_query.split(' ')
    if any(x in a_string for x in query_tokens):
        return True
    return False
    

def filter_services_by_field(query, services_list, field):
    # if its already short list, return
    query_lower = query.lower()
    # check that the saas-name includes the query
    tempList = services_list.copy()
    to_delete = []
    for idx in range(len(tempList)):
        if field in tempList[idx]:
            if len(tempList[idx][field]) > 0:
                # if query_lower not in tempList[idx][field].lower(): 
                if not query_in_string(query_lower, tempList[idx][field].lower()):                      
                    to_delete.append(idx)
    # delete all the entries without the query in the saasname
    to_delete.reverse()
    for idx in to_delete:
        del tempList[idx]    
    return tempList


def add_logo_and_url(services_list):
    """
    Add logo and url data for the given searices

    Parameters
    ----------
        services_list : list of dicts

    Returns
    -------
        services_list : updated services list 

    """
    # build the elk offload. notify: its 'should' not 'must...
    # offload = ' { "size" : ' + str(len(services_list)) + ','
    offload = '{ "size" : 100, "query": {"bool": {"should" : ['
    # add a line for each saas_name
    for saas in services_list:
        offload += '{"match": {"docId":"'+saas["saasNameForSearch"]+'"}},'
    # complete the offload
    offload = offload[:-1] + ']}}}'
    # send it to ELK and get response
    resp,data = get_logo_and_url_elk(offload)
    # for loop over all the services. init the new fields with empty string
    for idx in range(len(services_list)):
        cur_service = services_list[idx]
        cur_service['saasLogo'] = ''
        cur_service['saasUrl'] = ''
    
    # convert to dataframe
    services_dl =  pd.DataFrame(services_list)
    #for loop over the data got from the ELK. notify that it may be smaller size 
    # then the original searvice list
    for idx in range(len(data)):
        cur_data = data[idx]['_source']
        # get the searive index 
        services_idx = services_dl[services_dl.saasNameForSearch==cur_data['docId']].index[0]
        cur_service = services_list[services_idx]
        # update the new data
        if 'saasLogo' in cur_data:
            cur_service['saasLogo'] = cur_data['saasLogo']
        if 'saasUrl' in cur_data:            
            cur_service['saasUrl'] = cur_data['saasUrl']
    return services_list

        
def make_results(query, services_list):
    """   
    Make the results for the query.
    
    Parameters
    ----------
        query : string. the user query
        services_list : list of dicts 

    Returns
    -------
    results : updated list of services

    """
    # temp_services_list = []
    # # filter out service that have not description field or 
    # # their description not consist tokens from the query
    # if len(services_list) > results_length:
    #     temp_services_list = filter_services_by_field(query, 
    #                                              services_list,
    #                                              'description')
    # if len(temp_services_list) > 1:
    #     services_list = temp_services_list
    # make datafrmae sorted by weight    
    services_df = prepare_service_list_by_weight(services_list)
    # make dataframe of the first page
    services_df_page = services_df.head(results_length)
    # if dataframe consist of Nans, convert it to empty strings   
    services_df_page = services_df_page.fillna("")
    # make a list of dicts
    services_list = services_df_page.to_dict('records')
    # if description is Nan, convert it to empty string
    # for saas in services_list:
    #     if isNaN(saas['description']):
    #         saas['description'] = ''
    #     if isNaN(['saasIntegrations']):
    #         saas['saasIntegrations'] = ''
    # add logs and urls 
    # services_list = add_logo_and_url(services_list)
    n_results = len(services_list)
    results = {'nResults': n_results, 'query': query}
    # list_size = min(len(services_list),10)
    results['resultList'] = services_list
    return results
 

def saas_search(query, filters):
    # cleaned_query = query.lower()
    # to_search_list = cleaned_query.split(' ')  
    services_list = prepare_by_services(query, filters)   
    results = make_results(query, services_list)
    results['score'] = str(0)  
    return results   


def category_search(query,bert_embedding,cats_unique,filters):
    empty_results = {'nResults': 0, 'query': query}
    similar_results = process_bert_similarity(query, bert_embedding)
    score = similar_results[1]
    print('score = ',score)
    if score < score_threshold:
        empty_results['score'] = str(score)
        return empty_results
    idx = similar_results[0]
    to_search = cats_unique[idx][0]
    
    to_search_list = to_search.split(' ')
    for x in to_search_list:
        if len(x) <=1:
            to_search_list.remove(x)
        elif 'manag' in x :
            to_search_list.remove(x)
                      
    services_list = prepare_by_categories(to_search_list, filters)   
    # index_list = [i for i, lst in enumerate(cats) if to_search_list == lst]
    results =  make_results(query, services_list)
    results['score'] = str(score)
    return results
 

def numberOfVotes_to_number(NOV_str):
    nov_str = NOV_str.replace(',','')
    if nov_str[-1] == 'K':
        nov_int = int(float(nov_str.replace('K',''))*1000)
    else:
        nov_int = int(float(nov_str)) 
    return nov_int

    
def get_best_alternatives(alternatives_list):
    best_alter = []
    # alternatives_list = saas_entry['saasAlternativesList']
    alter_df = pd.DataFrame(alternatives_list)
    num_votes = []
    if 'numberOfVotes' in alter_df:
        num_votes =  alter_df['numberOfVotes'].to_list()
    for idx in range(len(num_votes)):
        num_votes[idx] = numberOfVotes_to_number(num_votes[idx])
    weights = [0] * len(num_votes)      
    for idx in range(len(num_votes)):
        weights[idx] = num_votes[idx] * float(alter_df['rating'].loc[idx])
    alter_df['weight'] = weights    
    alter_df = alter_df.sort_values(by=['weight'], ascending=False) 
    best_alter = alter_df.head(n=3)
    return best_alter    


def dict_to_json(dict, fn):
    with open(fn, "w") as outfile:
        json.dump(dict, outfile, indent = 4)
    return


def init_engine(): 
    global bert_embedding,text_list,cats_unique 
    
    elk_init()
    bert_embedding, text_list, cats_unique = read_bert_embedding() 
    print(' after read bert_embedding')   
    return


def user_search(query, filters=''):
    """
    entry pont for search engine. Clean the query and search as a category. 
    if success then done
    else do saas search (assuming query is a saas name)
    
    Parameters
    ----------
    query : string. got from the frongend

    Returns
    -------
    results : list of dicts.

    """
    # filter_dict= filter_parse(filters)
    query1 = clean_text(query)       
    results = category_search(query1, bert_embedding, cats_unique, filters)
    if results['nResults'] <= 0:
        results = saas_search(query, filters)
    # dict_to_json(results, query+'_results.json')
    return results    


def user_price(docId,saasName):
    result = prepare_price_by_saas(docId,saasName) 
    return result[0]
    

def user_integrations(docId,saasName):
    result = prepare_integrations_by_saas(docId,saasName) 
    return result


def test_search_from_list(text_list):
    all_results = {}
    for idx in range(len(text_list)):
        query = text_list[idx]
        print (query)
        results = user_search(query)
        all_results[query] = results.copy()
    # dict_to_json(all_results, 'all_results.json')
    return all_results


# def check_query_results():
#     query_results_df  = pd.read_csv('query_results.csv')
#     return 


def check_descriptions(descs_df):
    Typical_customers_size = 0
    Platforms_supported_size = 0
    Support_options_size = 0
    Training_options_size = 0
    found_labels = False
    for idx in range(len(descs_df)):
        found_labels = False    
        desc = descs_df.iloc[idx].description
        if '\nTypical customers:' in desc:
            Typical_customers_size += 1
            found_labels = True            
            
        if '\nPlatforms supported:' in desc:
            Platforms_supported_size += 1
            found_labels = True            

        if '\nSupport options:' in desc:
            Support_options_size += 1
            found_labels = True            

        if '\nTraining options:' in desc:
            Training_options_size += 1
            found_labels = True
            
        if not found_labels:
            print ("not found_labels")
        else:
            print ("found_labels")

            
    print( Typical_customers_size,
        Platforms_supported_size,
        Support_options_size,
        Training_options_size)
           
    return 


def check_cats(cats_df):
    no_cats_list = list()
    for idx in range(len(cats_df)):
        cur_cats = cats_df.iloc[idx].categories
        if cats_df.iloc[idx].saasNameForSearch == 'monday':
            print('monday')
        if len(cur_cats) <= 0:
            no_cats_entry = {'docId': cur_cats.docId, 
                             'saasName':cur_cats.saasNameForSearch}
            no_cats_list.append(no_cats_entry)
            print(len(no_cats_list))
    return no_cats_list
                     

def test_price():
    saasName = 'zoho crm'
    docId = 'getapp-zoho crm'
    price_dict = user_price(docId, saasName)
    return


def test_integrations():
    saasName = 'zoho crm'
    docId = 'getapp-zoho crm'
    integrations_list = user_integrations(docId, saasName)
    return


def test_descriptions():
    descs_df = prepare_with_field('description')
    check_descriptions(descs_df)
    return


def test_cats():
    descs_df = prepare_with_field('categories')
    check_cats(descs_df)
    return

    
def make_simple_execl(results):
    res_df = pd.DataFrame(results['resultList'])
    simple_df = res_df[['docId','saasUrl']]
    simple_df.to_csv(results['query']+'.csv')
    return

    # 'Customers' : 'typicalCustomers',
    # 'Platforms' : 'platformsSupported',
    # 'Support' : 'supportOptions',
    # 'Training' : 'trainingOptions'    
    
def test_search():
    query = "crm"  
    # filters = 'Customers=Freelancers,Small businesses&Platforms=all&Training=all&Support=Phone Support'
    # filters = 'Customers=Small businesses,Mid-size businesses'
    filters=''
    # filters = 'Customers=Freelancers,Small businesses'
    # query = "Sage" 
    # query = "PayPal Invoicing"
    results = user_search(query, filters)
    make_simple_execl(results)
    print('after user search')
    return

    
def build_unique_cats():
    cats_list = prepare_all_categories()
    cats_set = set()
    for categories in cats_list:
        cats_set.update(categories)
    unique_cats_list = list(cats_set)
    
    for idx in range(len(unique_cats_list)):
        if unique_cats_list[idx].endswith('software'):
            unique_cats_list[idx] = unique_cats_list[idx].replace(" software", "")
    new_unique = list(set(unique_cats_list))      
    return new_unique
       

def main():
    
    global bert_embedding,text_list,cats_unique
 
    if (EMBEDDING_MODE):
        save_embedding()

    # bert_embedding,text_list,cats_unique = init_engine()
    init_engine()
    # test_integrations()
    # build_unique_cats()
    # prepare_all_categories()
    test_search()
    # cats_df = prepare_with_field('categories')
    # no_cats_list = check_cats(cats_df)
    # test_search()
    elk_close()
    
    return

  
if __name__ == '__main__':
    main()
    print('end of work')    
    
