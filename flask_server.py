# -*- coding: utf-8 -*-
"""
Created on Sun Apr 24 14:40:23 2022

@author: reuve
"""

from flask import Flask, jsonify, request, redirect, url_for, render_template
# from flask_restful import Api, Resource
from search_app import search_saas_by_name,save_embedding,init_engine,user_search,user_price,user_integrations
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

# API = Api(app)
# # API.add_resource(Add, "/add")
# # API.add_resource(Substract, "/substract")

@app.route("/")
def index():
    # return '<html><body><h1>Hello World</h1></body></html>'  
    return render_template('index.html')


@app.route("/server")
def index_server():
    return render_template('index.html')


# @app.route('/hithere')
# def hi_there_everyone():
#     '''
#     basic function
#     '''
#     return "I just hit/hithere"


@app.route('/json')
def get_json(results):
    # '''
    # basic function
    # '''
    # retjson = {
    #     "name":"Aviad",
    #     "lastName":"Rivlin",
    #     "Age":42,
    #     "phones":[
    #         {
    #         "phoneName":"Samsung",
    #         "phoneNember":522224460
    #         },{
    #         "phoneName":"Samsung",
    #         "phoneNember":522224460
    #         }]}
    return jsonify(results)


@app.route('/got/<string>')
def got(string):
   return 'search for %s' % string


@app.route('/launch', methods = ['POST'])
def launch():
    # print ('init')
    save_embedding()
    init_engine()
    # redirect(url_for('server'))
    return render_template('index.html')


@app.route('/search',methods = ['POST', 'GET'])
def search():
   if request.method == 'POST':
      searchStr = request.form['searchStr']
      filters = request.form['filters']
   else:
      searchStr = request.args.get('searchStr')
      filters = request.args.get['filters']

   # redirect(url_for('got',string = searchStr))
   # print(searchStr)
   # print(filters)
   if len(filters) > 0:
       results = user_search(searchStr,filters)
   else:
       results = user_search(searchStr)
   # redirect(url_for('json'))         
   return jsonify(results)


@app.route('/price',methods = ['POST', 'GET'])
def price():
   if request.method == 'POST':
      saasName = request.form['saasName']
      docId = request.form['docId']
   else:
      saasName = request.args.get('saasName')
      docId = request.args.get('docId')

   # redirect(url_for('got',string = searchStr))
   results = user_price(docId,saasName)
   print (results)
   # redirect(url_for('json'))         
   return jsonify(results)


@app.route('/integrations',methods = ['POST', 'GET'])
def integrations():
   if request.method == 'POST':
      saasName = request.form['saasName']
      docId = request.form['docId']
   else:
      saasName = request.args.get('saasName')
      docId = request.args.get('docId')

   # redirect(url_for('got',string = searchStr))
   results = user_integrations(docId,saasName)
   print (results)
   # redirect(url_for('json'))         
   return jsonify(results)


@app.route('/get_by_name',methods = ['GET'])
def get_by_name():
    saasName = request.args.get('saasName')
    fields = request.args.get('fields')

    # redirect(url_for('got',string = searchStr))
    # results = user_price(docId,saasName)
    print (saasName)
    # redirect(url_for('json'))
    return jsonify(search_saas_by_name(saasName, fields))


if __name__ == '__main__':
   # app.run()
    save_embedding()
    init_engine()
    app.debug = True
    app.run(host="0.0.0.0", port=5000, debug=True)
